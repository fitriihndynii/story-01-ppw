from django.shortcuts import render

# Create your views here.

def myblog(request):
    return render(request, 'myblog.html')

def about(request):
    return render(request, 'about.html')

def background(request):
    return render(request, 'background.html')

def gallery(request):
    return render(request, 'gallery.html')

def message(request):
    return render(request, 'message.html')

def contact(request):
    return render(request, 'contact.html')